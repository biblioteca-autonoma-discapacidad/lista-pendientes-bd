# Operatividad y Seguridad



Los pasos faltantes para completar la funcionalidad completa y segura de la biblioteca son:

- [ ] **Definir nombre del dominio** (url) -> Esto es condición necesaria para aumentar la seguridad.
- [ ] Comprar nombre de dominio ($2.000 +/-) Está dentro del excedente del presupuesto para la compra del hardware.
- [ ] Instalar certificado SSL (Nivel 2) para dar seguridad de conexión a los colaboradores y visitantes.
- [ ] Proteger Biblioteca de posibles ataques con Clouflare (Nivel 2).



**Es importante la definición del nombre del dominio (o enlace) para poder avanzar en los demás puntos de seguridad.**

