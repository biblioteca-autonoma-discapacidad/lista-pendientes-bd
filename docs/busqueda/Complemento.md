# Búsqueda Avanzada con complemento



La búsqueda avanzada de Omeka con un complemento especializado presenta esta vista:

<p align="center">
     <img src="https://gitlab.com/krv00/lista-pendientes-bd/-/raw/master/docs/img/complemento01.png" width="700" />
</p>



Y los resultados de las búsqueda tiene este formato:

<p align="center">
     <img src="https://gitlab.com/krv00/lista-pendientes-bd/-/raw/master/docs/img/complemento02.png" width="700" />
</p>

