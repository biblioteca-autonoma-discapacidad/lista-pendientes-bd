<p align="center">
     <img src="https://gitlab.com/krv00/lista-pendientes-bd/-/raw/master/docs/img/Logo_ColectDiscapacidad.png" width="150" />
</p>
# Biblioteca Autónoma de la Discapacidad

## Lista de pendientes

Documento con el listado de pendientes para la publicación de la biblioteca de acuerdo al trabajo realizado por el [Colectivo Disonancia](https://krv00.gitlab.io/lista-pendientes-bd/cd/).

## Evaluación de complejidad

Para referirse al grado de complejidad de elaboración, se propone un **criterio de 3 niveles**.

   - **1** Cambio de carácter simple en la administración de Omeka
  
   - **2** Cambio complejo que requiere modificar el código del servidor
    
   - **3** Cambio de elevada complejidad que requiere desarrollo (tiempo indeterminado)

## Biblioteca

La dirección provisoria de la biblioteca es:

[http://bibliotecadiscap.ddns.net/omeka/](http://bibliotecadiscap.ddns.net/omeka/)

## Archive.org

Cuenta de Archive.org de la biblioteca es:

[@biblioteca_autonoma_discapacidad](https://archive.org/details/@biblioteca_autonoma_discapacidad)